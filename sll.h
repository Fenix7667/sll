/**
 * Author: Fenix7667
 * License: The GPLv3 License (GPLv3)
 *
 * (c) Copyright by Fenix7667.
 **/

#ifndef SLL_H
#define SLL_H

#include <stddef.h>

struct sll_node
{
	struct sll_node *next;
	void *value;
};

struct sll_container
{
	struct sll_node *head;
	size_t num_elements;
};

struct sll_node *sll_new_node(void *value);
struct sll_container *sll_new_container(struct sll_node *head);
struct sll_node *sll_get_node(struct sll_container *container, size_t pos);

void sll_set_node(struct sll_container *container, struct sll_node *new_node, size_t pos);
void sll_remove_node(struct sll_container *container, size_t pos);
void sll_free_container(struct sll_container *container);

#endif /* SLL_H */
