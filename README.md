# SLL - Simple Linked List
SLL is an implementation of a stack-based linked list written in C. SLL allows it's users to store any data via void pointers.

Example of how to use it:
```c
int head_value = 2, node_value = 100;

struct sll_node *head_node = sll_new_node(&head_value);
struct sll_node *my_node = sll_new_node(&node_value);

struct sll_container *my_container = sll_new_container(head_node);

/* position 1 is always the next position after the head of the container. */
sll_set_node(my_container, my_node, 1);
	
/* position 0 is always the head of the container. Value of node 0: 2 */
printf("value of node 0: %i\n", *(int *)sll_get_node(my_container, 0)->value);
/* value of node 1: 100 */
printf("value of node 1: %i\n", *(int *)sll_get_node(my_container, 1)->value);

/* Free everything */
sll_free_container(my_container);
```

Documentation of functions:
```c
sll_new_container -> returns a new container and a head node for that container with the default value of int 1.
sll_new_node -> returns a new node with the next node being NULL and the default value of int 1. 

sll_get_node -> returns a node from the linked list in a container and position.
sll_set_node -> returns nothing but sets a node to a position in the container linked list.

sll_remove_node -> returns nothing but removes a node in a position from a container linked list.
sll_free_container -> returns nothing but frees every node in a container and also the container.
```

Dependencies: libc.
