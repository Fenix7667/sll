/**
 * Author: Fenix7667
 * License: The GPLv3 License (GPLv3)
 *
 * (c) Copyright by Fenix7667.
 **/
 
#include <stdio.h>
#include <stdlib.h>

#include "sll.h"

struct sll_node *sll_new_node(void *value)
{
	struct sll_node *new_node = malloc(sizeof(struct sll_node));
	new_node->next = NULL;
	new_node->value = value;
	return new_node;
}

struct sll_container *sll_new_container(struct sll_node *head) 
{
	struct sll_container *container = malloc(sizeof(struct sll_container));
	container->head = head;
	container->num_elements = 1;
	return container;
}

struct sll_node *sll_get_node(struct sll_container *container, size_t pos)
{
	if (pos < 0) {
		return NULL; 
	}
	struct sll_node *node = container->head;
	for (size_t index = 0; index < pos; index++) {
		node = node->next;
	}
	return node;
}

void sll_set_node(struct sll_container *container, struct sll_node *new_node, size_t pos)
{
	if (pos -1 < 0) {
		return;
	}
	struct sll_node *node = sll_get_node(container, pos -1);
	if (node->next == NULL) {
		container->num_elements++;
	}
	new_node->next = node->next;
	node->next = new_node;
}

void sll_remove_node(struct sll_container *container, size_t pos)
{
	if (pos -1 < 0) {
		return;
	}
	struct sll_node *node = sll_get_node(container, pos -1);
	if (node == NULL) {
		return;
	}
	if (node->next != NULL) {
		container->num_elements--;
	}
	node->next = node->next->next;
}

void sll_free_container(struct sll_container *container)
{
	struct sll_node *current_node = container->head;
	while (current_node != NULL) {
		struct sll_node *node_for_free = current_node;
		current_node = current_node->next;
		free(node_for_free);
	}
	free(container);
}
